Rails.application.routes.draw do
  get 'static_pages/index'

  get 'static_pages/about'

  get 'static_pages/contact'

  get '/not_found', to: "errors#not_found", via: :all
  get '/internal_server_error', to: "errors#not_found", via: :all

  devise_for :users

  root 'static_pages#index'
  get '/schools', to: 'schools#index', as: 'schools'
  get '/schools/:id', to: 'schools#show', as: 'school'
  resources :courses, only: [:index, :show]
  resources :posts, only: [:show]
  get 'join_courses/:id', to: 'join_courses#create', as: 'join_course'
  resources :students
  namespace 'admin' do
    get 'dashboard', to: 'dashboard#index'
    resources :schools
    resources :posts
    resources :lessons, only: [:show]
    resources :courses do
      resources :lessons
    end

  end
end

require 'rails_helper'

RSpec.describe Student, type: :model do
  let(:student) { FactoryGirl.build :student }
  
  subject { student }

  context 'has respond to these attributes' do
    it { should respond_to :first_name }
    it { should respond_to :last_name }
    it { should respond_to :age }
    it { should respond_to :rg }
    it { should respond_to :birthdate }
  end

  context 'has validations' do
    it { should validate_presence_of :first_name }
    it { should validate_presence_of :last_name }
    it { should validate_presence_of :rg }
    it { should validate_presence_of :age }
    it { should validate_presence_of :birthdate }
    it { should validate_numericality_of :rg }
    it { should validate_numericality_of :age }
  end

  context 'has relationships' do
    it { should belong_to :user }
    it { should have_many :course_enrollments }
    it { should have_many :courses }
  end
end

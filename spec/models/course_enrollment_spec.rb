require 'rails_helper'

RSpec.describe CourseEnrollment, type: :model do
  let(:course_enrollment) { FactoryGirl.build :course_enrollment }

  subject { course_enrollment }
  
  it { should belong_to :student }
  it { should belong_to :course }

  it { should validate_associated :student }
end

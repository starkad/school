require 'rails_helper'

RSpec.describe Course, type: :model do
  let(:course) { FactoryGirl.build(:course) }

  subject { course }

  context 'has respond for these attributes' do
    it { should respond_to :name }
    it { should respond_to :price }
    it { should respond_to :duration }
    it { should respond_to :description }
  end

  context 'has validations' do
    it { should validate_presence_of :name }
    it { should validate_presence_of :price }
    it { should validate_presence_of :name }
    it { should validate_numericality_of :price }
    it { should be_valid }
  end

  context 'has relationships' do
    it { should belong_to :school }
    it { should have_many :lessons }
    it { should have_many :students }
    it { should have_many :course_enrollments }
  end
end

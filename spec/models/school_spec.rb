require 'rails_helper'

RSpec.describe School, type: :model do

  before { @school = FactoryGirl.build(:school) }

  subject { @school }

  it { should respond_to :name }
  it { should respond_to :foundation }
  it { should respond_to :subdomain }
  it { should respond_to :pitch }
  it { should respond_to :email }
  it { should belong_to :user }
  it { should have_many :courses }
  it { should be_valid }

 context 'Some validation to School' do
    it { should validate_presence_of :name }
    it { should validate_presence_of :foundation }
    it { should validate_presence_of :subdomain }
    it { should validate_presence_of :pitch }
    it { should validate_presence_of :email }
    it { should allow_value('escola.edools.com').for(:subdomain) }
  end

end

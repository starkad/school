require 'rails_helper'

RSpec.describe Post, type: :model do
  let(:post) { FactoryGirl.build(:post) }

  subject { post }

  it { should respond_to :title }
  it { should respond_to :content }
  it { should belong_to :user }
end

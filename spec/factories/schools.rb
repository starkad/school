FactoryGirl.define do
  factory :school do
    name "MyString"
    subdomain "MyString"
    pitch "MyString"
    foundation "2017-03-03"
    email "MyString"
  end
end

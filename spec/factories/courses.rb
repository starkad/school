FactoryGirl.define do
  factory :course do
    name "MyString"
    price "9.99"
    description "MyText"
  end
end

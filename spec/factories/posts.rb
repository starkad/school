FactoryGirl.define do
  factory :post do
    title "MyString"
    content "MyText"
    published false
    user_id '1'
  end
end

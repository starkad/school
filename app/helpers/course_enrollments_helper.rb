module CourseEnrollmentsHelper

  def is_joined(course)
    current_user.student.courses.where('course_id = ?', course.id).blank?
  end
end

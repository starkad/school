module Admin::SchoolsHelper
  def active_students(school)
    active = 0
    school.courses.each do |course|
      active = active + course.students.count
    end
    active
  end
end

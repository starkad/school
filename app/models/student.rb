class Student < ApplicationRecord
  validates :first_name, :last_name, :birthdate, presence: true
  validates :age, :rg, presence: true, numericality: { only_integer: true, greater_than: 0 }

  belongs_to :user

  has_many :course_enrollments
  has_many :courses, through: :course_enrollments
end

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_one :school
  has_one :student
  has_many :posts

  has_many :course_enrollments
  has_many :courses, through: :course_enrollments
end

class School < ApplicationRecord
  belongs_to :user
  has_many :courses
  validates :name, uniqueness: true, presence: true
  validates :pitch, :foundation,:subdomain, presence: true
  validates :email, presence: true, format: Devise.email_regexp
  validates :subdomain, format: { with: /\A\w+\.(edools.com)\z/i }

  scope :by_name, ->(name) { where('lower(name) LIKE ?', "%#{name}%") }
end

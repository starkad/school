class Post < ApplicationRecord
  belongs_to :user

  scope :is_published, -> { where('published is true') }
end

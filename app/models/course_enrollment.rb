class CourseEnrollment < ApplicationRecord
  belongs_to :student
  belongs_to :course

  validates :student, :course, associated: true
end

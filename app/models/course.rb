class Course < ApplicationRecord
  validates :name, :duration, :price, presence: true
  validates :price, numericality: { only_float: true, greater_than: 0.0 }

  has_many :lessons
  has_many :course_enrollments
  has_many :students, through: :course_enrollments
  belongs_to :school

end

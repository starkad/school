class SchoolsController < ApplicationController
  before_action :set_school, only: [:show]
  before_action :authenticate_user!, only: [:show]
  def index
    @search = School.search(params[:q])
    @schools = @search.result.page params['page']
  end

  def show
    @search_course = @school.courses.search(params[:q])
    @results = @search_course.result
  end

  private
  def set_school
    @school = School.find(params[:id])
  end
end

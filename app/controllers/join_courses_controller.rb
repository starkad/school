class JoinCoursesController < ApplicationController
  before_action :set_course, only: [:create, :show]
  def create
    @join_course = CourseEnrollment.create(student_id: current_user.student.id,
                                           course_id: @course.id)
    @join_course.save
    flash[:notice] = 'Matriculado com sucesso'
    redirect_to @course
  end

  private

  def set_course
    @course = Course.find(params[:id])
  end

  def params_enrollment
    params.require(:course_enrollment).permit(:course_id, :student_id)
  end
end

class CoursesController < ApplicationController
  before_action :set_course, only: [:show, :join_course]
  before_action :authenticate_user!, only: [:show]

  def index
    @c = Course.search(params[:q])
    @courses = @c.result.page params['page']
  end

  def show ; end

  private

  def set_course
    @course = Course.find(params[:id])
  end
end

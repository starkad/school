module Admin
  class CoursesController < AdminController
    before_action :set_course, only: [:edit, :destroy, :update, :show]

    def index
      if !current_user.school.nil?
        @courses = current_user.school.courses.all
      else
        redirect_to new_admin_school_url, alert: 'Voce precisa cadastrar uma escola.'
      end
    end

    def show; end

    def new
      @course = Course.new
    end

    def create
      @course = current_user.school.courses.new(course_params)
      if @course.save
        redirect_to admin_course_path(@course)
      else
        render 'new'
      end
    end

    def edit; end

    def update
      if @course.update(course_params)
        redirect_to admin_courses_path
      else
        render 'edit'
      end
    end

    def destroy
      @course.destroy
      redirect_to admin_courses_url
    end

    private

    def course_params
      params.require(:course).permit(:name, :duration, :price, :description)
    end

    def set_course
      @course = Course.find(params[:id])
    end
  end
end

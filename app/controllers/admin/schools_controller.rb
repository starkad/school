module Admin
  class SchoolsController < AdminController
    before_action :set_school, only: [:show, :edit, :update, :destroy]
    def index
      @schools = current_user.school
    end

    def new
      if !current_user.school
        @school = School.new
      end
    end

    def create
      @school = current_user.build_school(school_params)
      if @school.save
        redirect_to admin_school_url(@school)
      else
        render 'new'
      end
    end

    def show; end

    def edit; end

    def update
      if @school.update(school_params)
        flash[:notice] = 'Escola atualizada com sucesso.'
        redirect_to admin_schools_url
      else
        render 'edit'
      end
    end

    private

    def set_school
      @school = School.find(params[:id])
    end

    def school_params
      params.require(:school).permit(:name, :subdomain, :foundation,
                                     :email, :pitch)
    end
  end
end

module Admin
  class AdminController < ApplicationController
    before_filter :authenticate_admin_user!

    protected
    def authenticate_admin_user!
      if user_signed_in?
        redirect_to not_found_url unless current_user.is_admin
      else
        redirect_to not_found_url
      end
    end

  end
end

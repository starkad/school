class StaticPagesController < ApplicationController
  def index
    @posts = Post.is_published.order(:created_at)
  end

  def about
  end

  def contact
  end
end

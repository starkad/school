class StudentsController < ApplicationController
  def index
    @students = Students.all
  end
  
  def new
    @student = Student.new
  end

  def create
    @student = current_user.build_student(student_params)
    if @student.save
      redirect_to student_path(@student)
    else
      render 'new'
    end
  end

  def show ; end

  def edit ; end

  def update
  end
  
  def destroy
  end

  private
  def set_student
  end

  def student_params
    params.require(:student).permit(:rg, :age, :birthdate, :last_name, :first_name)
  end
end

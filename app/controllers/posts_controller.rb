class PostsController < ApplicationController
  def index
    @posts = Post.is_published.order_by(:created_at).desc
  end

  def show
    @post = Post.find(params[:id])
  end

end

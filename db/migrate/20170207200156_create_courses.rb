class CreateCourses < ActiveRecord::Migration[5.0]
  def change
    create_table :courses do |t|
      t.string :name
      t.decimal :price, precision: 7, scale: 2, default: 0.0
      t.text :description
      t.string :duration, default: ""

      t.timestamps
    end
  end
end

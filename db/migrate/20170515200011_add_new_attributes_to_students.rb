class AddNewAttributesToStudents < ActiveRecord::Migration[5.0]
  def change
    add_column :students, :rg, :string, null: false, uniqueness: true
    add_column :students, :age, :integer, null: false
    add_column :students, :birthdate, :date, null: false
    add_column :students, :first_name, :string, null: false
    add_column :students, :last_name, :string, null: false
  end
end

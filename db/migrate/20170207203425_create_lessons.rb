class CreateLessons < ActiveRecord::Migration[5.0]
  def change
    create_table :lessons do |t|
      t.string :title
      t.integer :book_chapter
      t.integer :course_id

      t.timestamps
    end
    add_index :lessons, :course_id
  end
end

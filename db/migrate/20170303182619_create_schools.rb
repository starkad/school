class CreateSchools < ActiveRecord::Migration[5.0]
  def change
    create_table :schools do |t|
      t.integer :user_id, uniqueness: true
      t.string :name
      t.string :subdomain
      t.string :pitch
      t.date :foundation
      t.string :email

      t.timestamps
    end
  end
end
